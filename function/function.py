def foo1(value: int, size=1):
    if value>size:
        foo(value, size*10)
    print((value%(size*10))//size)

def foo(value: int, size=1):
    return foo(value, size*10)+' '+str(value%(size*10)//size) if value>=size else ''
