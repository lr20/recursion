from function.function import foo

def test_foo_1():
    number = 250
    assert foo(number) == ' 2 5 0'

def test_foo_2():
    number = 49762
    assert foo(number) == ' 4 9 7 6 2'

def test_foo_3():
    number = 1
    assert foo(number) == ' 1'